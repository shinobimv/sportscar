
 //import Inmemory db services
import { InMemoryDbService } from 'angular-in-memory-web-api';
//export inmemory data class
export class InMemoryDataService implements InMemoryDbService {
  //creating db
  createDb() {
    //array cass
    const cars = [
      { id: 0,  name: 'Ferrari', des: 'Ferrari luxury automotive brand.', img: 'app/img/ferrari.jpg' },
      { id: 1, name: 'Aston Martin', des: 'Aston Martin is a name synonymous with style and luxury and is one of the best luxury car manufacturers in the world.', img: 'app/img/astonmartin.jpg' },
      { id: 2, name: 'Audi', des: 'Audi has a long history filled with numerous difficulties.', img: 'app/img/audi.jpg' },
      { id: 3, name: 'Bugatti', des: 'Bugatti, one of the best car manufacturers in the world today, was launched way back in 1885', img: 'app/img/bugatti.jpg' },
      { id: 4, name: 'Chevrolet', des: 'Chevrolet is designed with speed in mind', img: 'app/img/chevrolet.jpg' },
      { id: 5, name: 'McLaren', des: 'McLaren is a luxury car', img: 'app/img/mclaren.jpg' },
      { id: 6, name: 'Mercedes', des: 'Mercedes is a fast car', img: 'app/img/mercedes.jpg' }


    ];
    //return cars
    return {cars};
  }
}
