

 //import components
import { Component }          from '@angular/core';
//component function
@Component({
  //selectpr my-app
  selector: 'my-app',
  //app.component js file source code 
  template: `
  <div class='panel-body'> <nav class='navbar navbar-inverse'>  
     <h1 style='color:white;'>. <b>{{title}}</b></h1>
    
      <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
      <a routerLink="/cars" routerLinkActive="active">Cars</a>
    <div >   </div></nav>
    <router-outlet></router-outlet> </div> 
  `,
  //CSS file location for the file 
  styleUrls: ['./app.component.css']
})
//export class for applciation title
export class AppComponent {
  title = 'Sports Cars';
}
